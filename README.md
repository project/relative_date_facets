# Relative Date Facets
This module is the spiritual Drupal 9 successor to  the [Date Facets](https://www.drupal.org/project/date_facets) module.

## Overview
This module provides date range facets similar to major search engines. The UI is provided as a [Facets](http://drupal.org/project/facets) widget called "Relative date range".

## Usage
After installing this module, all date facets should have the "Relative Date Range" widget available in the facet's display settings page. It is recommended that all facet sort settings are unchecked so that the items are displayed in the intended order.

## Maintainer
Relative Date Facets is maintained by __Kevin Rice__. Development of this module is sponsored by [IQ Solutions](https://iqsolutions.com).