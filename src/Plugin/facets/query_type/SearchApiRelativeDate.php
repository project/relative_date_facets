<?php

namespace Drupal\relative_date_facets\Plugin\facets\query_type;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\facets\QueryType\QueryTypeRangeBase;
use Drupal\facets\Result\Result;

/**
 * Support for relative date facets within the Search API scope.
 *
 * This query type supports dates for all possible backends. This specific
 * implementation of the query type supports a generic solution of adding facets
 * for relative dates.
 *
 * @FacetsQueryType(
 *   id = "search_api_relative_date",
 *   label = @Translation("Relative Date"),
 * )
 */
class SearchApiRelativeDate extends QueryTypeRangeBase {

  /**
   * The current time as a Drupal date time object.
   *
   * @var \Drupal\Core\Datetime\DrupalDateTime
   */
  private $currentTime;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $facet = $configuration['facet'];
    $processors = $facet->getProcessors();
    $dateProcessorConfig = $processors['relative_date_item']->getConfiguration();

    $configuration = $this->getConfiguration();
    $relativeDates = $dateProcessorConfig['relative_dates_container']['relative_dates'];
    $resultFilters = [];
    $this->currentTime = new DrupalDateTime();

    foreach ($relativeDates as $relativeDate) {
      $date = !empty($relativeDate['length']) ? new DrupalDateTime($relativeDate['length']) : NULL;
      $resultFilter = [
        'display' => $relativeDate['label'],
        'raw' => !is_null($date) ? $date->format('Y-m-d') : NULL,
      ];

      $resultFilters[$resultFilter['raw']] = new Result($this->facet, $resultFilter['raw'], $resultFilter['display'], 0);
    }

    $configuration['static_filters'] = $resultFilters;

    $this->setConfiguration($configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function calculateRange($value) {
    $filter_date = DrupalDateTime::createFromFormat('Y-m-d', $value);

    $range = [];

    if ($this->currentTime <= $filter_date) {
      $range['start'] = $this->currentTime->format('U');
      $range['stop'] = $filter_date->format('U');
    }
    else {
      $range['stop'] = $this->currentTime->format('U');
      $range['start'] = $filter_date->format('U');
    }

    return $range;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateResultFilter($value) {}

  /**
   * {@inheritdoc}
   */
  public function build() {
    // If there were no results or no query object, we can't do anything.
    if (empty($this->results)) {
      return $this->facet;
    }

    $query_operator = $this->facet->getQueryOperator();
    /** @var \Drupal\facets\Result\ResultInterface[] $facetResults */
    $facetResults = $this->getConfiguration()['static_filters'];
    foreach ($this->results as $result) {
      // Go through the results and add facet results grouped by filters
      // defined by self::calculateResultFilter().
      if ($result['count'] || $query_operator == 'or') {
        $count = $result['count'];
        $fieldDate = DrupalDateTime::createFromTimestamp(trim($result['filter'], '"'));

        foreach ($facetResults as $facetResult) {
          $facetDate = DrupalDateTime::createFromTimestamp(strtotime($facetResult->getRawValue()));

          if (($facetDate <= $fieldDate && $fieldDate <= $this->currentTime)
            || ($facetDate >= $fieldDate && $fieldDate >= $this->currentTime)
          ) {
            $newCount = $facetResult->getCount() + $count;
            $facetResult->setCount($newCount);
          }
        }
      }
    }

    $this->facet->setResults($facetResults);
    return $this->facet;
  }

}
