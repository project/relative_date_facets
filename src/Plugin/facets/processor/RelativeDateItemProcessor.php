<?php

namespace Drupal\relative_date_facets\Plugin\facets\processor;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\facets\FacetInterface;
use Drupal\facets\Processor\BuildProcessorInterface;
use Drupal\facets\Processor\ProcessorPluginBase;

/**
 * Provides a processor for relative dates.
 *
 * @FacetsProcessor(
 *   id = "relative_date_item",
 *   label = @Translation("Customizable relative date item processor"),
 *   description = @Translation("Display facets relative to the current date for date fields."),
 *   stages = {
 *     "build" = 35
 *   }
 * )
 */
class RelativeDateItemProcessor extends ProcessorPluginBase implements BuildProcessorInterface {

  /**
   * {@inheritDoc}
   */
  public function build(FacetInterface $facet, array $results) {
    return $results;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'relative_dates_container' => [
        'relative_dates' => [
          [
            'label' => '',
            'length' => '',
          ],
        ],
      ],
      'cache_max_age' => 0,
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state, FacetInterface $facet) {

    $relativeDates = $this->getConfiguration()['relative_dates_container']['relative_dates'];
    $range_count = $form_state->get('range_count');

    if ($range_count === NULL) {
      if (empty($relativeDates)) {
        $form_state->set('range_count', 1);
      }
      else {
        $form_state->set('range_count', count($relativeDates));
      }

      $range_count = $form_state->get('range_count');
    }

    $build['#tree'] = TRUE;

    $build['relative_dates_container'] = [
      '#type' => 'container',
      '#title' => $this->t('Add relative date ranges'),
      '#prefix' => '<div id="relative-dates-wrapper">',
      '#suffix' => '</div>',
    ];

    for ($i = 0; $i < $range_count; $i++) {
      $build['relative_dates_container']['relative_dates'][$i] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Relative date range'),
      ];

      $label = $relativeDates[$i]['label'] ?? '';

      $build['relative_dates_container']['relative_dates'][$i]['label'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Range Label'),
        '#default_value' => $label,
      ];

      $url = Url::fromUri('https://www.php.net/manual/en/datetime.formats.relative.php');
      $length = $relativeDates[$i]['length'] ?? '';

      $build['relative_dates_container']['relative_dates'][$i]['length'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Range Length'),
        '#description' => $this->t('Field expects conventions found at @link. Example "-1 week" will create a facet that has a range between now and one week ago', [
          '@link' => Link::fromTextAndUrl('PHP Relative Formats', $url)->toString(),
        ]),
        '#default_value' => $length,
      ];
    }

    $build['relative_dates_container']['actions'] = [
      '#type' => 'actions',
    ];

    $build['relative_dates_container']['actions']['add_range'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add another range'),
      '#submit' => [
        [
          self::class,
          'addOne',
        ],
      ],
      '#ajax' => [
        'callback' => [
          self::class,
          'addRangeCallback',
        ],
        'wrapper' => 'relative-dates-wrapper',
      ],
    ];

    // If there is more than one name, add the remove button.
    if ($range_count > 1) {
      $build['relative_dates_container']['actions']['remove_range'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove one'),
        '#submit' => [
          [
            self::class,
            'removeRangeCallback',
          ],
        ],
        '#ajax' => [
          'callback' => [
            self::class,
            'addRangeCallback',
          ],
          'wrapper' => 'relative-dates-wrapper',
        ],
      ];
    }

    $build['cache_max_age'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cache max-age'),
      '#size' => '25',
      '#maxlength' => '30',
      '#description' => $this->t('Length of time in seconds facet should be cached.'),
      '#default_value' => $this->getConfiguration()['cache_max_age']
    ];

    return $build;
  }

  /**
   * Callback for both ajax-enabled buttons.
   *
   * Selects and returns the fieldset with the names in it.
   */
  public static function addRangeCallback(array &$form, FormStateInterface $form_state) {
    $relativeDatesElement = $form['facet_settings']['relative_date_item']['settings']['relative_dates_container'];
    return $relativeDatesElement;
  }

  /**
   * Submit handler for the "add-one-more" button.
   *
   * Increments the max counter and causes a rebuild.
   */
  public static function addOne(array &$form, FormStateInterface $form_state) {
    $name_field = $form_state->get('range_count');
    $add_button = $name_field + 1;
    $form_state->set('range_count', $add_button);
    // Since our buildForm() method relies on the value of 'num_names' to
    // generate 'name' form elements, we have to tell the form to rebuild. If we
    // don't do this, the form builder will not call buildForm().
    $form_state->setRebuild();
  }

  /**
   * Submit handler for the "remove one" button.
   *
   * Decrements the max counter and causes a form rebuild.
   */
  public function removeRangeCallback(array &$form, FormStateInterface $form_state) {
    $name_field = $form_state->get('range_count');
    if ($name_field > 1) {
      $remove_button = $name_field - 1;
      $form_state->set('range_count', $remove_button);
    }
    // Since our buildForm() method relies on the value of 'num_names' to
    // generate 'name' form elements, we have to tell the form to rebuild. If we
    // don't do this, the form builder will not call buildForm().
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function getQueryType() {
    return 'relative_date';
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge(): int {
    return $this->getConfiguration()['cache_max_age'];
  }

}
